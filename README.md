# ldraw_landscape.html - Introduction

ldraw_ldandscape.html is a standalone HTML/ECMAScript application to generate
LDraw models (.LDR) of landscapes.

By “standalone application” we mean that the user just needs to open the file
ldraw_landscape.html in their (recent) browser.  No web server needed.  No
other installation than downloading the files.

ldraw_landscape.html is licenced under GPLv3+.


# Requirements

* NONE!
